package wiwo

import (
	"fmt"
	"net/http"
)

// ResponseError extends the error interface by adding a getter for the HTTP
// status code.
type ResponseError interface {
	error

	// GetStatusCode returns the HTTP status code set in the implementing error.
	GetStatusCode() int
}

// ValidationError must provide a method to set the validation field errors to
// the error object and return a valid ResponseError.
type ValidationError interface {
	// SetfieldErrors sets the validation field errors and returns a valid
	// ResponseError.
	SetFieldErrors([]FieldError) ResponseError
}

// Error is the default WIWO response error structure.
type Error struct {
	// StatusCode set to the response when this error occurs.
	StatusCode int `json:"-" xml:"-"`

	// Code is the human readable error code.
	Code string `json:"error_code" xml:"ErrorCode"`

	// Msg is a descriptive error message.
	Msg string `json:"error_message" xml:"ErrorMessage"`

	// Details about the error
	Details interface{} `json:"details,omitempty": xml:"Details"`
}

// Error to implement the stdlib error.
// Provides the `code: msg` error.
func (err Error) Error() string {
	return fmt.Sprintf("%s: %s", err.Code, err.Msg)
}

// GetStatusCode returns the HTTP status code se in the error.
func (err Error) GetStatusCode() int {
	return err.StatusCode
}

// SetFieldErrors sets the validation field errors to the Details member and
// returns a clone of itself.
func (err Error) SetFieldErrors(fieldErrors []FieldError) ResponseError {
	err.Details = fieldErrors
	return err
}

var (
	// ErrNoContentType is used when an attempt to decode a request or encode a
	// response is made without a set ContentType. One should either be set in
	// the WIWO Config, or determined from the Content-Type or Accept headers.
	ErrNoContentType ResponseError = Error{
		StatusCode: http.StatusInternalServerError,
		Code:       "no_contenttype_error",
		Msg:        "An error in the server configuration prevented the request from being processed.",
	}

	// ErrRequestBodyRead is used when an error occurs while reading the raw
	// request body.
	ErrRequestBodyRead ResponseError = Error{
		StatusCode: http.StatusInternalServerError,
		Code:       "request_read_error",
		Msg:        "An error occurred reading the request body.",
	}

	// ErrDecode is used when an error occurs while decoding the raw request
	// body.
	ErrDecode ResponseError = Error{
		StatusCode: http.StatusBadRequest,
		Code:       "body_decode_error",
		Msg:        "Unable to decode request body.",
	}

	// ErrEncode is used when an error occurs while encoding the response data.
	ErrEncode ResponseError = Error{
		StatusCode: http.StatusInternalServerError,
		Code:       "response_encode_error",
		Msg:        "Unable to encode response data.",
	}

	// ErrNoWIWORequest is used when the call to GetRequest is made and WIWO
	// Request object was not set to the request context.
	ErrNoWIWORequest ResponseError = Error{
		StatusCode: http.StatusInternalServerError,
		Code:       "no_request_error",
		Msg:        "An unexpected error has prevented the server from fulfilling the request.",
	}

	// ErrValidationError is used when an error occurs in request data
	// validation. The error is usually also populated with FieldError data.
	ErrValidationError ValidationError = Error{
		StatusCode: http.StatusUnprocessableEntity,
		Code:       "validation_error",
		Msg:        "Server could not process the request due to invalid request data.",
	}
)
