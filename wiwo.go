package wiwo

import (
	"context"
	"net/http"
	"reflect"
)

// ContentType provides an encoder and decoder getters used to encode and decode
// data, a media type getter for the Content-Type HTTP header value, and a check
// method to help determine if a string value is a valid Content-Type HTTP
// header value of the implementing ContentType object.
type ContentType interface {
	// Is determines if the received Content-Type string value is a valid
	// Content-Type for the implementing data type.
	Is(ct string) bool

	// GetMediaType returns a media type for the Content-Type HTTP header value.
	GetMediaType() string

	// GetEncoder returns the Encoder function that is used by WIWO to encode
	// the data before writing it to the response.
	GetEncoder() Encoder

	// GetDecoder returns the Decoder function that is used by WIWO to decode
	// the data received in the request.
	GetDecoder() Decoder
}

// Encoder function to encode the outpu data.
type Encoder func(interface{}) ([]byte, error)

// Decoder function to decode the input data.
type Decoder func([]byte, interface{}) error

// Middleware creates a new WIWO request and attempts to decode the request
// body. WIWO must be used as middleware and takes an instance of the object to
// which the parsed request body is decoded. The decoded object is then added to
// the WIWO request and passed on to the next handler thought the request
// context.
// To access the WIWO request and the decoded object use the GetRequest
// function.
// Although the function accepts multiple data objects, only the first one is
// used.
func Middleware(data ...interface{}) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			wiwoRequest := NewRequest(cfg.DefaultContentType, r)

			if len(data) > 0 {
				defer r.Body.Close()
				objType := reflect.TypeOf(data[0]).Elem()
				obj := reflect.New(objType).Interface()

				if err := wiwoRequest.decode(r.Body, obj); err != nil {
					writeError(w, r, err)
					return
				}

				if err := ValidateRequestData(obj); err != nil {
					writeError(w, r, err)
					return
				}
			}

			ctx := context.WithValue(r.Context(), requestCtxKey, wiwoRequest)
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)

			resp := GetResponse(r)
			if resp == nil {
				return
			}

			resp.Write(w)
		}

		return http.HandlerFunc(fn)
	}
}

func writeError(w http.ResponseWriter, r *http.Request, respErr error) {
	resp := NewResponse(WithResponseError(respErr))
	err := resp.SetToRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp.Write(w)
}
