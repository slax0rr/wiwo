package wiwo

import (
	"encoding/json"
	"regexp"
)

// JSON data type.
type JSON struct{}

// Is checks if the received Content-Type string is a valid JSON Content-Type.
func (j JSON) Is(ct string) bool {
	return regexp.MustCompile(`(?i:(application|text)/(json|.*\+json|json\-.*)(;|$))`).MatchString(ct)
}

// GetMediaType returns a media type for the Content-Type HTTP header value.
func (j JSON) GetMediaType() string {
	return "application/json"
}

// GetEncoder returns the Encoder function for JSON data type.
// The stdlib json.Marshal function is used as it satisfies the Encoder type.
func (j JSON) GetEncoder() Encoder {
	return json.Marshal
}

// GetDecoder returns the Decoder function for JSON data type.
// The stdlib json.Unmarshal function is used as it satisfies the Decoder type.
func (j JSON) GetDecoder() Decoder {

	return json.Unmarshal
}
