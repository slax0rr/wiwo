package wiwo

import (
	"net/http"
	"net/url"
	"time"

	"github.com/spf13/cast"
)

// Query struct containing the raw url.Values map.
type Query struct {
	params url.Values
}

// NewQuery inits the Query struct by copying the url.Values from the http.Request
// URL and returning an object of it.
func NewQuery(r *http.Request) *Query {
	q := new(Query)
	q.params = r.URL.Query()
	return q
}

// Get returns the first query parameter value belonging to the specified key
// as a string.
func (q Query) Get(key string) string {
	return q.params.Get(key)
}

// GetAll returns all query parameter values belonging to the specified key as a
// string slice.
func (q Query) GetAll(key string) []string {
	return q.params[key]
}

// GetInt returns the query parameter value belonging to the specified key
// as a int.
func (q Query) GetInt(key string) int {
	return cast.ToInt(q.params.Get(key))
}

// GetAllInt returns all query parameter values belonging to the specified key
// as a int slice.
func (q Query) GetAllInt(key string) []int {
	stringVals := q.params[key]
	vals := make([]int, len(stringVals))
	for i := range stringVals {
		vals[i] = cast.ToInt(stringVals[i])
	}
	return vals
}

// GetInt32 returns the first query parameter value belonging to the specified
// key as a int32.
func (q Query) GetInt32(key string) int32 {
	return cast.ToInt32(q.params.Get(key))
}

// GetAllInt32 returns all query parameter values belonging to the specified key
// as a int32 slice.
func (q Query) GetAllInt32(key string) []int32 {
	stringVals := q.params[key]
	vals := make([]int32, len(stringVals))
	for i := range stringVals {
		vals[i] = cast.ToInt32(stringVals[i])
	}
	return vals
}

// GetInt64 returns the first query parameter value belonging to the specified
// key as a int64.
func (q Query) GetInt64(key string) int64 {
	return cast.ToInt64(q.params.Get(key))
}

// GetAllInt64 returns all query parameter values belonging to the specified key
// as a int64 slice.
func (q Query) GetAllInt64(key string) []int64 {
	stringVals := q.params[key]
	vals := make([]int64, len(stringVals))
	for i := range stringVals {
		vals[i] = cast.ToInt64(stringVals[i])
	}
	return vals
}

// GetFloat32 returns the first query parameter value belonging to the specified
// key as a float32.
func (q Query) GetFloat32(key string) float32 {
	return cast.ToFloat32(q.params.Get(key))
}

// GetAllFloat32 returns all query parameter values belonging to the specified
// key as a float32 slice.
func (q Query) GetAllFloat32(key string) []float32 {
	stringVals := q.params[key]
	vals := make([]float32, len(stringVals))
	for i := range stringVals {
		vals[i] = cast.ToFloat32(stringVals[i])
	}
	return vals
}

// GetFloat64 returns the first query parameter value belonging to the specified
// key as a float64.
func (q Query) GetFloat64(key string) float64 {
	return cast.ToFloat64(q.params.Get(key))
}

// GetAllFloat64 returns all query parameter values belonging to the specified
// key as a float64 slice.
func (q Query) GetAllFloat64(key string) []float64 {
	stringVals := q.params[key]
	vals := make([]float64, len(stringVals))
	for i := range stringVals {
		vals[i] = cast.ToFloat64(stringVals[i])
	}
	return vals
}

// GetBool returns the first query parameter value belonging to the specified
// key as a bool.
func (q Query) GetBool(key string) bool {
	return cast.ToBool(q.params.Get(key))
}

// GetAllBool returns all query parameter values belonging to the specified key
// as a bool slice.
func (q Query) GetAllBool(key string) []bool {
	stringVals := q.params[key]
	vals := make([]bool, len(stringVals))
	for i := range stringVals {
		vals[i] = cast.ToBool(stringVals[i])
	}
	return vals
}

// GetTime returns the first query parameter value belonging to the specified
// key as a time.Time.
//
// The format of the time in the query parameter value is guessed in the
// following order:
//     - 2006-01-02T15:04:05Z07:00
//     - 2006-01-02T15:04:05
//     - Mon, 02 Jan 2006 15:04:05 -0700
//     - Mon, 02 Jan 2006 15:04:05 MST
//     - 02 Jan 06 15:04 -0700
//     - 02 Jan 06 15:04 MST
//     - Monday, 02-Jan-06 15:04:05 MST
//     - Mon Jan _2 15:04:05 2006
//     - Mon Jan _2 15:04:05 MST 2006
//     - Mon Jan 02 15:04:05 -0700 2006
//     - 2006-01-02 15:04:05.999999999 -0700 MST
//     - 2006-01-02
//     - 02 Jan 2006
//     - 2006-01-02T15:04:05-0700
//     - 2006-01-02 15:04:05 -07:00
//     - 2006-01-02 15:04:05 -0700
//     - 2006-01-02 15:04:05Z07:00
//     - 2006-01-02 15:04:05Z0700
//     - 2006-01-02 15:04:05
//     - 3:04PM
//     - Jan _2 15:04:05
//     - Jan _2 15:04:05.000
//     - Jan _2 15:04:05.000000
//     - Jan _2 15:04:05.000000000
//     - Unix timestamp
func (q Query) GetTime(key string) time.Time {
	//param := q.params.Get(key)
	param := q.params[key][0]

	time, err := cast.ToTimeE(param)
	if err == nil {
		return time
	}

	return cast.ToTime(cast.ToInt64(param))
}

// GetAllTime returns all query parameter values belonging to the specified key
// as a time.Time slice.
//
// The format of the time in the query parameter value is guessed in the
// following order:
//     - 2006-01-02T15:04:05Z07:00
//     - 2006-01-02T15:04:05
//     - Mon, 02 Jan 2006 15:04:05 -0700
//     - Mon, 02 Jan 2006 15:04:05 MST
//     - 02 Jan 06 15:04 -0700
//     - 02 Jan 06 15:04 MST
//     - Monday, 02-Jan-06 15:04:05 MST
//     - Mon Jan _2 15:04:05 2006
//     - Mon Jan _2 15:04:05 MST 2006
//     - Mon Jan 02 15:04:05 -0700 2006
//     - 2006-01-02 15:04:05.999999999 -0700 MST
//     - 2006-01-02
//     - 02 Jan 2006
//     - 2006-01-02T15:04:05-0700
//     - 2006-01-02 15:04:05 -07:00
//     - 2006-01-02 15:04:05 -0700
//     - 2006-01-02 15:04:05Z07:00
//     - 2006-01-02 15:04:05Z0700
//     - 2006-01-02 15:04:05
//     - 3:04PM
//     - Jan _2 15:04:05
//     - Jan _2 15:04:05.000
//     - Jan _2 15:04:05.000000
//     - Jan _2 15:04:05.000000000
//     - Unix timestamp
func (q Query) GetAllTime(key string) []time.Time {
	stringVals := q.params[key]
	vals := make([]time.Time, len(stringVals))

	var err error
	for i, sv := range stringVals {
		vals[i], err = cast.ToTimeE(sv)
		if err != nil {
			vals[i] = cast.ToTime(cast.ToInt64(sv))
		}

	}

	return vals
}

// GetDuration returns the first query parameter value belonging to the
// specified key as a time.Duration.
//
// If the query parameter value does not contain a time unit and is just a
// number it is presumed to be a nanosecond (ns).
func (q Query) GetDuration(key string) time.Duration {
	return cast.ToDuration(q.params.Get(key))
}

// GetAllDuration returns all query parameter values belonging to the specified
// key as a time.Duration slice.
//
// If the query parameter value does not contain a time unit and is just a
// number it is presumed to be a nanosecond (ns).
func (q Query) GetAllDuration(key string) []time.Duration {
	stringVals := q.params[key]
	vals := make([]time.Duration, len(stringVals))
	for i := range stringVals {
		vals[i] = cast.ToDuration(stringVals[i])
	}
	return vals
}
