package wiwo

// contextKey used in the net/http request context.
type contextKey string

// String returns the prepended context key name.
func (k contextKey) String() string {
	return "slax0rr/wiwo: " + string(k)
}

const (
	// requestCtxKey holds the WIWO Request data in the net/http request context.
	requestCtxKey = contextKey("request data")

	// responseCtxKey holds the WIWO Response data in the net/http request context.
	responseCtxKey = contextKey("response data")
)
