# WIWO

WIWO is Yoruban for **interface**, and also stands for Web Input/Output. It is
an library for Go, making your life easier by, helping you to decode incoming
requests, validate the decoded data, and properly encode your response data.

## Documentation

Documentation of the library is available at
[pkg.go.dev](https://pkg.go.dev/gitlab.com/slax0rr/wiwo?tab=doc).

## Usage

The library provides a **Middleware handler** that you can put to use before
your actual handler, and it will attempt determine which decoder to use to
decode the request body, and later if you set a response that same middleware
will encode your response and send it.

## Request

To get the decoded request, simply pass a pointer of the type to which you want
to decode to, and later use `wiwo.GetRequest(*http.Request)` function to get the
WIWO request which will hold your decoded data in your handler:

```go
package main

import (
    "net/http"

    "gitlab.com/slax0rr/wiwo"
)

type SomeData struct {
    Foo string `json:"foo"`
    Bar string `json:"bar"`
}

func main() {
    mux := http.NewServeMux()
    mux.Handle("/", wiwo.Middleware(&SomeData{})(
        http.HandlerFunc(func w http.ResponseWriter, r *http.Request) {
            req := wiwo.GetRequest(r)
            data := req.Data.(SomeData)
            // do stuff
        },
    ))

    err := http.ListenAndServe(":8080", mux)
    if err != nil {
        panic(err)
    }
}
```

## Response

To send a response all you need to do is create it and add it to the request:

```go
wiwo.NewResponse(
    wiwo.WithData(data),
).SetToRequest(r)
```

The WIWO middleware will catch that data, encode it, and send it. If no default
`ContentType` is set, and if you don't pass a `WithContentType` option to the
Response factory, WIWO will attempt to find a matching `ContentType`
implementation to satisfy the **Accept** HTTP header.

## Force a ContentType

If you don't want to let WIWO attempt to determine which decoder/encoder to use,
you can set a default `ContentType` which provides a decoder and encoder
functions:

```go
wiwo.SetConfig(&wiwo.Config{
    DefaultContentType: wiwo.JSON{},
})
```

### Available content types and matching order

WIWO currently provides the following Content Type definitions:

* JSON
* XML

And by default WIWO will attempt to match the requests Content-Type header in the following
order:

* JSON
* XML

You can however override this order using with config:

```go
wiwo.SetConfig(&wiwo.Config{
    ContentTypes: []wiwo.ContentType{
        wiwo.XML{},
        wiwo.JSON{},
    }
})
```

## Data validation

If the request data object implements the `Validator` interface:

```go
type Validator interface {
	Validate() error
}
```

WIWO will call this function after decoding the request if the `Validation` has
not been set to `false` in the configuration. If the **Validate** function
returns a *non-nil* error value the configured `ValidationParser` will be called
with the error to convert the error to a `[]FieldError` slice containing
detailed validation errors, which is then printed to the response inside the
`ErrValidationError` object.

WIWO provides the validation parser for the
[go-playground/validator](https://github.com/go-playground/validator), but you
can use any kind of validation for your data, as long as you provide the parser
function for it in the configuration. If no parser is defined, then a plain
validation error is printed to response without details.
