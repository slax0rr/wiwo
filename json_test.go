package wiwo_test

import (
	"fmt"
	"testing"

	"github.com/remogatto/prettytest"
	"gitlab.com/slax0rr/wiwo"
)

type jsonSuite struct {
	prettytest.Suite
}

func (t *jsonSuite) TestIsJSON() {
	json := wiwo.JSON{}

	for _, tc := range []struct {
		ct  string
		exp bool
	}{
		{"application/json", true},
		{"application/json; charset=utf-8", true},
		{"application/anything+json", true},
		{"application/anything+json; charset=utf-8", true},
		{"application/xml+json", true},
		{"application/xml+json; charset=utf8", true},
		{"application/json-anything", true},
		{"application/json-anything; charset=utf8", true},

		{"text/json", true},
		{"text/json; charset=utf-8", true},
		{"text/anything+json", true},
		{"text/anything+json; charset=utf-8", true},
		{"text/xml+json", true},
		{"text/xml+json; charset=utf8", true},
		{"text/json-anything", true},
		{"text/json-anything; charset=utf8", true},

		{"application/json+anything", false},
		{"application/json.anything", false},
		{"application/anything.json", false},
		{"application/anything-json", false},

		{"text/json+anything", false},
		{"text/json.anything", false},
		{"text/anything.json", false},
		{"text/anything-json", false},
	} {
		t.Equal(tc.exp, json.Is(tc.ct),
			fmt.Sprintf("tested content type (%s) did not yield expected check state: %t", tc.ct, !tc.exp))
	}
}

func TestJSON(t *testing.T) {
	prettytest.RunWithFormatter(
		t,
		new(prettytest.TDDFormatter),
		new(jsonSuite),
	)
}
