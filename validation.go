package wiwo

import "github.com/go-playground/validator/v10"

// Validator provides a Validate function which validates the validity of the
// object data.
type Validator interface {
	// Validate validates the data and returns any errors.
	Validate() error
}

// FieldError holds the detailed data on why the validation failed.
type FieldError struct {
	// Field name which failed to validate.
	Field string `json:"field" xml:"Field"`

	// Rule contains the validation rule that failed.
	Rule string `json:"rule" xml:"Rule"`

	// Namespace returns the namespace of the field that failed to validate.
	Namespace string `json:"namespace" xml:"Namespace"`
}

// ValidationParser function parses the validation errors and returns the
// FieldError slice.
type ValidationParser func(error) []FieldError

// ParsePlaygroundValidatorErrors parses the validation errors from the
// go-playground/validator errors and returns a FieldError slice.
func ParsePlaygroundValidatorErrors(err error) []FieldError {
	vldErr, ok := err.(validator.ValidationErrors)
	if !ok {
		return nil
	}

	fieldErrors := make([]FieldError, len(vldErr))

	for i, e := range vldErr {
		fieldErrors[i] = FieldError{
			Field:     e.Field(),
			Rule:      e.Tag(),
			Namespace: e.Namespace(),
		}
	}

	return fieldErrors
}

// ValidateRequestData runs the validation if config permits it and the data
// objects implements the Validator interface.
// If the config does not permit validation or the data object does not
// implement the Validator interface, no error is returned.
func ValidateRequestData(data interface{}) error {
	if !cfg.Validation {
		return nil
	}

	vld, ok := data.(Validator)
	if !ok {
		return nil
	}

	err := vld.Validate()
	if err != nil {
		var fieldErrors []FieldError
		if cfg.ValidationParser != nil {
			fieldErrors = cfg.ValidationParser(err)
		}
		return ErrValidationError.SetFieldErrors(fieldErrors)
	}

	return nil
}
