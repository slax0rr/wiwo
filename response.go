package wiwo

import (
	"context"
	"fmt"
	"net/http"
)

// Response struct holding the response data, the status code, headers and the
// ContentType object used to encode the data.
type Response struct {
	// contentType providing the encoder and default Content-Type header
	contentType ContentType

	// statusCode to be set for the HTTP response.
	statusCode int

	// header for the response
	header http.Header

	// data for the response.
	data interface{}

	// written indicates the response has already been written.
	written bool
}

// GetResponse returns the response set to the request context.
// If the response was not set, just nil is returned.
func GetResponse(r *http.Request) *Response {
	resp, _ := r.Context().Value(responseCtxKey).(*Response)
	return resp
}

// ResponseOpt function is used to set various options to the Request object
// when creating it.
type ResponseOpt func(*Response)

// NewResponse initialises the Response object and applies the received opts
// by executing each ResponseOpt function and returns the Response object.
func NewResponse(opts ...ResponseOpt) *Response {
	resp := new(Response)

	for _, opt := range opts {
		opt(resp)
	}

	return resp
}

// WithContentType option sets the ct ContentType to the Response object used
// for encoding the response data and setting the Content-Type HTTP header.
func WithContentType(ct ContentType) ResponseOpt {
	return func(resp *Response) {
		resp.contentType = ct
	}
}

// WithStatusCode sets the HTTP status code to the Response object.
func WithStatusCode(code int) ResponseOpt {
	return func(resp *Response) {
		resp.statusCode = code
	}
}

// WithHeader sets the net/http.Header to the Response object.
func WithHeader(header http.Header) ResponseOpt {
	return func(resp *Response) {
		resp.header = header
	}
}

// WithData sets your response data to the Response object which is encoded and
// written to the HTTP response.
func WithData(data interface{}) ResponseOpt {
	return func(resp *Response) {
		resp.data = data
	}
}

// WithResponseError sets the error as the response data which is encoded and
// written to the HTTP response.
func WithResponseError(err error) ResponseOpt {
	return func(resp *Response) {
		if respErr, ok := err.(ResponseError); ok {
			resp.statusCode = respErr.GetStatusCode()
		}
		resp.data = err
	}
}

// SetToRequest sets the Response object to the net/http.Request context in
// order to later read it in the WIWO Middleware and process it.
func (resp *Response) SetToRequest(r *http.Request) error {
	if resp.contentType == nil {
		resp.setContentType(cfg.DefaultContentType, r)
	}

	newReq := r.WithContext(context.WithValue(r.Context(), responseCtxKey, resp))
	*r = *newReq

	return nil
}

// Write the response to the http.ResponseWriter.
// If an error occurs during encoding and a ResponseError is returned, that
// error gets written as text/plain to the response.
func (resp *Response) Write(w http.ResponseWriter) error {
	if resp.written {
		return nil
	}
	resp.written = true

	body, err := resp.encode()
	if err != nil {
		if respErr, ok := err.(ResponseError); ok {
			w.WriteHeader(respErr.GetStatusCode())
			w.Write([]byte(respErr.Error()))
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
		return err
	}

	for header, values := range resp.getHeaders() {
		for _, v := range values {
			w.Header().Add(header, v)
		}
	}

	w.WriteHeader(resp.statusCode)
	w.Write(body)

	return nil
}

// setContentType sets the ContentType object to the Response. If the received
// ContentType is nil, then the request Accept header is inspected in order to
// determine which ContentType to use.
// Order of checking is defined in the Config by `ContentTypes`.
func (resp *Response) setContentType(ct ContentType, r *http.Request) error {
	if ct != nil {
		resp.contentType = ct
		return nil
	}

	req, err := GetRequest(r)
	if err != nil {
		return err
	}

	accept, ok := req.header["Accept"]
	if !ok {
		return fmt.Errorf("unable to guess the content type, Accept header not set")
	}

Accept:
	for _, a := range accept {
		for _, ct := range cfg.ContentTypes {
			if ct.Is(a) {
				resp.contentType = ct
				break Accept
			}
		}
	}

	return nil
}

// encode the data encodes the Response data with the set ContentType.
func (resp *Response) encode() ([]byte, error) {
	if resp.data == nil {
		// no data set, nothing to encode
		return nil, nil
	}

	if resp.contentType == nil {
		return nil, ErrNoContentType
	}

	data, err := resp.contentType.GetEncoder()(resp.data)
	if err != nil {
		return nil, ErrEncode
	}

	return data, nil
}

// getHeaders return the Response object headers.
// If the Content-Type header is not set it will be set to the ContentType
// objects media type.
func (resp *Response) getHeaders() http.Header {
	if resp.data == nil {
		return resp.header
	}

	if len(resp.header["Content-Type"]) == 0 && resp.contentType != nil {
		if resp.header == nil {
			resp.header = http.Header{}
		}
		resp.header.Set("Content-Type", resp.contentType.GetMediaType())
	}

	return resp.header
}
