package wiwo

import (
	"encoding/xml"
	"regexp"
)

// XML data type.
type XML struct{}

// Is checks if the received Content-Type string is a valid XML Content-Type.
func (x XML) Is(ct string) bool {
	return regexp.MustCompile(`(?i:(application|text)/(xml|.*\+xml)(;|$))`).MatchString(ct)
}

// GetMediaType returns a media type for the Content-Type HTTP header value.
func (x XML) GetMediaType() string {
	return "application/xml"
}

// GetEncoder returns the Encoder function for XML data type.
// The stdlib xml.Marshal function is used as it satisfies the Encoder type.
func (x XML) GetEncoder() Encoder {

	return xml.Marshal
}

// GetDecoder returns the Decoder function for XML data type.
// The stdlib xml.Unmarshal function is used as it satisfies the Decoder type.
func (x XML) GetDecoder() Decoder {

	return xml.Unmarshal
}
