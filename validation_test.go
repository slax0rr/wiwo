package wiwo_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/slax0rr/wiwo"
)

type validationSuite struct {
	suite.Suite
}

func (t *validationSuite) TestValidateRequestData() {
	testCases := []struct {
		config *wiwo.Config
		data   interface{}
		err    error
	}{
		{
			config: &wiwo.Config{Validation: false},
			data:   validationData{},
		},
		{
			config: &wiwo.Config{Validation: true},
			data:   struct{ Foo string }{Foo: "bar"},
		},
		{
			config: &wiwo.Config{Validation: true},
			data: validationData{
				validator: func() error {
					return nil
				},
			},
		},
		{
			config: &wiwo.Config{
				Validation: true,
				ValidationParser: func(err error) []wiwo.FieldError {
					return nil
				},
			},
			data: validationData{
				validator: func() error {
					return fmt.Errorf("test validation error")
				},
			},
			err: wiwo.ErrValidationError.SetFieldErrors(nil),
		},
		{
			config: &wiwo.Config{
				Validation: true,
				ValidationParser: func(err error) []wiwo.FieldError {
					return []wiwo.FieldError{
						{
							Field:     "foo",
							Rule:      "bar",
							Namespace: "baz",
						},
					}
				},
			},
			data: validationData{
				validator: func() error {
					return fmt.Errorf("test validation error")
				},
			},
			err: wiwo.ErrValidationError.SetFieldErrors([]wiwo.FieldError{
				{
					Field:     "foo",
					Rule:      "bar",
					Namespace: "baz",
				},
			}),
		},
	}

	for _, tc := range testCases {
		wiwo.SetConfig(tc.config)

		err := wiwo.ValidateRequestData(tc.data)
		if tc.err == nil {
			assert.Nil(t.T(), err)
		} else {
			respErr, ok := err.(wiwo.Error)
			if !ok {
				t.T().Errorf("expected error to be wiwo.Error, got: %#v", err)
				continue
			}

			expErr := tc.err.(wiwo.Error)

			assert.Equal(t.T(), expErr.StatusCode, respErr.StatusCode)
			assert.Equal(t.T(), expErr.Code, respErr.Code)
			assert.Equal(t.T(), expErr.Msg, respErr.Msg)
			assert.Equal(t.T(), expErr.Details, respErr.Details)
		}
	}
}

func TestValidation(t *testing.T) {
	suite.Run(t, new(validationSuite))
}
