package wiwo

// Config to control the behaviour of the WIWO library.
type Config struct {
	// DefaultContentType to use for decoding request data, and encoding of
	// response data. Leave empty if you want to let WIWO guess based on the
	// requests Content-Type or Accept HTTP header values.
	DefaultContentType ContentType

	// ContentTypes and their order in which WIWO will try to match the
	// Content-Type request header.
	ContentTypes []ContentType

	// Validation determines if the request data will be validated if the object
	// implements the Validator interface.
	Validation bool

	// ValidationParser is the function used to parse the validation errors.
	ValidationParser ValidationParser
}

var (
	// cfg is the set config for the WIWO library. By default it already has a
	// value set, but can be overwritten with SetConfig.
	cfg = &Config{
		ContentTypes:     []ContentType{&JSON{}},
		Validation:       true,
		ValidationParser: ParsePlaygroundValidatorErrors,
	}
)

// SetConfig sets the cfg variable.
func SetConfig(config *Config) {
	cfg = config
}
