package wiwo

import (
	"io"
	"io/ioutil"
	"net/http"
)

// Request object holding the decoded request body.
type Request struct {
	// contentType providing the decoder to decode the raw request body.
	contentType ContentType

	// header holds the HTTP headers set from the stdlib *http.Request.
	header http.Header

	// Data holds the decoded request body object.
	Data interface{}
}

// GetRequest returns the WIWO request from the HTTP request context.
// If the WIWO request has not been set to the HTTP request context and error is
// returned.
func GetRequest(r *http.Request) (*Request, error) {
	req, ok := r.Context().Value(requestCtxKey).(*Request)
	if !ok {
		return nil, ErrNoWIWORequest
	}
	return req, nil
}

// NewRequest creates a new WIWO request and determines a decoder to be used if
// the received 'decoder' is nil.
func NewRequest(ct ContentType, r *http.Request) *Request {
	req := new(Request)
	req.header = r.Header
	req.contentType = ct

	if req.contentType == nil {
		header := r.Header.Get("Content-Type")
		for _, ct := range cfg.ContentTypes {
			if ct.Is(header) {
				req.contentType = ct
				break
			}
		}
	}

	return req
}

// decode the body to the received data object and set it as Data in the Request
// object.
func (r *Request) decode(body io.ReadCloser, data interface{}) error {
	if r.contentType == nil {
		return ErrNoContentType
	}

	b, err := ioutil.ReadAll(body)
	if err != nil {
		return ErrRequestBodyRead
	}

	err = r.contentType.GetDecoder()(b, data)
	if err != nil {
		return ErrDecode
	}

	r.Data = data

	return nil
}
