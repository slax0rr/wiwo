package wiwo_test

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/slax0rr/wiwo"
)

type wiwoSuite struct {
	suite.Suite
}

func (t *wiwoSuite) TestWIWO() {
	mux := http.NewServeMux()

	server := httptest.NewServer(mux)
	defer server.Close()

	testCases := []struct {
		route     string
		reqBody   string
		data      interface{}
		reqHeader http.Header
		handler   func(http.ResponseWriter, *http.Request)
		cfg       *wiwo.Config
		respCode  int
		respBody  string
		respCT    string
	}{
		// test if the response is properly encoded when content type is set to the new response
		{
			route: "/test/resp/setct",
			handler: func(w http.ResponseWriter, r *http.Request) {
				data := testData{Foo: "test data"}

				wiwo.NewResponse(
					wiwo.WithContentType(wiwo.JSON{}),
					wiwo.WithStatusCode(http.StatusCreated),
					wiwo.WithData(data),
				).SetToRequest(r)
			},
			cfg: &wiwo.Config{
				ContentTypes: []wiwo.ContentType{wiwo.JSON{}},
			},
			respCode: http.StatusCreated,
			respBody: `{"foo":"test data"}`,
			respCT:   "application/json",
		},
		// test if the response is properly encoded if content type is not set but the request has an Accept header set
		{
			route: "/test/resp/accepthdr",
			reqHeader: http.Header{
				"Accept": []string{"application/xml", "application/json"},
			},
			handler: func(w http.ResponseWriter, r *http.Request) {
				data := testData{Foo: "test data"}

				wiwo.NewResponse(
					wiwo.WithStatusCode(http.StatusAccepted),
					wiwo.WithData(data),
				).SetToRequest(r)
			},
			cfg: &wiwo.Config{
				ContentTypes: []wiwo.ContentType{wiwo.JSON{}},
			},
			respCode: http.StatusAccepted,
			respBody: `{"foo":"test data"}`,
			respCT:   "application/json",
		},
		// test if the response is properly encoded with the default content type set and the content type header overridden in response
		{
			route: "/test/resp/defaultct",
			handler: func(w http.ResponseWriter, r *http.Request) {
				data := testData{Foo: "test data"}

				wiwo.NewResponse(
					wiwo.WithHeader(http.Header{"Content-Type": []string{"application/json; charset=utf-8"}}),
					wiwo.WithStatusCode(http.StatusAccepted),
					wiwo.WithData(data),
				).SetToRequest(r)
			},
			cfg: &wiwo.Config{
				DefaultContentType: wiwo.JSON{},
			},
			respCode: http.StatusAccepted,
			respBody: `{"foo":"test data"}`,
			respCT:   "application/json; charset=utf-8",
		},
		// test the request is properly decoded when default content type is not set
		{
			route:   "/test/req/nodefault",
			reqBody: `{"foo":"test"}`,
			data:    &testData{},
			reqHeader: http.Header{
				"Content-Type": []string{"application/json; charset=utf-8"},
				"Accept":       []string{"application/xml", "application/json"},
			},
			handler: func(w http.ResponseWriter, r *http.Request) {
				req, err := wiwo.GetRequest(r)
				if err != nil {
					t.T().Error(err)
					return
				}

				if td, ok := req.Data.(*testData); ok {
					assert.Equal(t.T(), "test", td.Foo)
				} else {
					t.T().Error("request data not found or invalid form")
					return
				}

				wiwo.NewResponse(
					wiwo.WithStatusCode(http.StatusNoContent),
				).SetToRequest(r)
			},
			cfg: &wiwo.Config{
				ContentTypes: []wiwo.ContentType{wiwo.JSON{}},
			},
			respCode: http.StatusNoContent,
		},
		// test the request is properly decoded when the default content type is set
		{
			route:   "/test/req/default",
			reqBody: `{"foo":"test"}`,
			data:    &testData{},
			handler: func(w http.ResponseWriter, r *http.Request) {
				req, err := wiwo.GetRequest(r)
				if err != nil {
					t.T().Error(err)
					return
				}

				if td, ok := req.Data.(*testData); ok {
					assert.Equal(t.T(), "test", td.Foo)
				} else {
					t.T().Error("request data not found or invalid form")
					return
				}

				wiwo.NewResponse(
					wiwo.WithStatusCode(http.StatusNoContent),
				).SetToRequest(r)
			},
			cfg: &wiwo.Config{
				DefaultContentType: wiwo.JSON{},
			},
			respCode: http.StatusNoContent,
		},
		// test the request decoding error is properly written to the response
		{
			route:   "/test/req/decodeerr",
			reqBody: `{"foo":test}`,
			data:    &testData{},
			handler: func(w http.ResponseWriter, r *http.Request) {},
			cfg: &wiwo.Config{
				DefaultContentType: wiwo.JSON{},
			},
			respCode: http.StatusBadRequest,
			respBody: `{"error_code":"body_decode_error","error_message":"Unable to decode request body."}`,
			respCT:   "application/json",
		},
		// test the request decoding error without content type to ensure an error is still written to the response.
		{
			route:    "/test/req/decodeerr/noct",
			reqBody:  `{"foo":test}`,
			data:     &testData{},
			handler:  func(w http.ResponseWriter, r *http.Request) {},
			cfg:      &wiwo.Config{},
			respCode: http.StatusInternalServerError,
			respBody: `no_contenttype_error: An error in the server configuration prevented the request from being processed.`,
			respCT:   "text/plain; charset=utf-8",
		},
		// test validation errors are properly parsed and written to response
		{
			route:   "/test/req/vlderr",
			reqBody: `{"foo":""}`,
			data:    &validationData{},
			handler: func(w http.ResponseWriter, r *http.Request) {},
			cfg: &wiwo.Config{
				Validation:         true,
				ValidationParser:   wiwo.ParsePlaygroundValidatorErrors,
				DefaultContentType: wiwo.JSON{},
			},
			respCode: http.StatusUnprocessableEntity,
			respBody: `{"error_code":"validation_error","error_message":"Server could not process the request due to invalid request data.","details":[{"field":"Foo","rule":"required","namespace":"validationData.Foo"}]}`,
			respCT:   "application/json",
		},
	}

	for _, tc := range testCases {
		if tc.data == nil {
			mux.Handle(tc.route, wiwo.Middleware()(http.HandlerFunc(tc.handler)))
		} else {
			mux.Handle(tc.route, wiwo.Middleware(tc.data)(http.HandlerFunc(tc.handler)))
		}

		if tc.cfg != nil {
			wiwo.SetConfig(tc.cfg)
		}

		req, err := http.NewRequest("POST", server.URL+tc.route, bytes.NewBufferString(tc.reqBody))
		if err != nil {
			t.T().Error(err)
			continue
		}

		if len(tc.reqHeader) > 0 {
			req.Header = tc.reqHeader
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.T().Error(err)
			continue
		}
		defer resp.Body.Close()

		assert.Equal(t.T(), tc.respCode, resp.StatusCode)

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.T().Error(err)
			continue
		}
		assert.Equal(t.T(), tc.respBody, string(body))

		assert.Equal(t.T(), tc.respCT, resp.Header.Get("Content-Type"))
	}
}

func (t *wiwoSuite) TestConsecutiveCallValidation() {
	mux := http.NewServeMux()

	server := httptest.NewServer(mux)
	defer server.Close()

	wiwo.SetConfig(&wiwo.Config{
		Validation:         true,
		ValidationParser:   wiwo.ParsePlaygroundValidatorErrors,
		DefaultContentType: wiwo.JSON{},
	})

	mux.Handle(
		"/test/req/vlderr",
		wiwo.Middleware(&validationData{})(
			http.HandlerFunc(
				func(w http.ResponseWriter, r *http.Request) {
				},
			),
		),
	)

	testCases := []struct {
		body string
		code int
	}{
		{
			body: `{"foo":"bar"}`,
			code: http.StatusOK,
		},
		{
			body: `{}`,
			code: http.StatusUnprocessableEntity,
		},
	}

	for _, tc := range testCases {
		resp, err := http.Post(
			server.URL+"/test/req/vlderr",
			"application/json",
			bytes.NewBufferString(tc.body),
		)
		if err != nil {
			t.T().Error(err)
			return
		}

		assert.Equal(t.T(), tc.code, resp.StatusCode)
	}
}

func TestWIWO(t *testing.T) {
	suite.Run(t, new(wiwoSuite))
}

type testData struct {
	Foo string `json:"foo"`
}

type validationData struct {
	validator func() error

	Foo string `json:"foo" validate:"required"`
}

func (d validationData) Validate() error {
	if d.validator != nil {
		return d.validator()
	}
	return validator.New().Struct(d)
}

type mockContentType struct {
	mock.Mock
}

func (m mockContentType) Is(ct string) bool {
	args := m.Called(ct)
	return args.Get(0).(bool)
}

func (m mockContentType) GetMediaType() string {
	args := m.Called()
	return args.Get(0).(string)
}

func (m mockContentType) GetEncoder() wiwo.Encoder {
	args := m.Called()
	return args.Get(0).(wiwo.Encoder)
}

func (m mockContentType) GetDecoder() wiwo.Decoder {
	args := m.Called()
	return args.Get(0).(wiwo.Decoder)
}
