module gitlab.com/slax0rr/wiwo

go 1.13

require (
	github.com/go-playground/validator/v10 v10.2.0
	github.com/remogatto/prettytest v0.0.0-20200211072524-6d385e11dcb8
	github.com/spf13/cast v1.3.1
	github.com/stretchr/testify v1.5.1
)
