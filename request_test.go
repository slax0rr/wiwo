package wiwo_test

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/slax0rr/wiwo"
)

type requestSuite struct {
	suite.Suite
}

func (t *requestSuite) TestGetRequest() {
	req, err := http.NewRequest("GET", "/", nil)
	assert.Nil(t.T(), err, "unable to create test HTTP request: %v", err)

	_, err = wiwo.GetRequest(req)
	assert.NotNil(t.T(), err, "expected an error since no wiwo request was set, got nil")
}

func TestRequest(t *testing.T) {
	suite.Run(t, new(requestSuite))
}
