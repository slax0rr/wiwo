package wiwo_test

import (
	"fmt"
	"testing"

	"github.com/remogatto/prettytest"
	"gitlab.com/slax0rr/wiwo"
)

type xmlSuite struct {
	prettytest.Suite
}

func (t *xmlSuite) TestIsXML() {
	xml := wiwo.XML{}

	for _, tc := range []struct {
		ct  string
		exp bool
	}{
		{"application/xml", true},
		{"application/xml; charset=utf-8", true},
		{"application/json+xml", true},
		{"application/json+xml; charset=utf-8", true},
		{"application/vnd.anything+xml", true},
		{"application/vnd.anything+xml; charset=utf-8", true},

		{"text/xml", true},
		{"text/xml; charset=utf-8", true},
		{"text/json+xml", true},
		{"text/json+xml; charset=utf-8", true},
		{"text/vnd.anything+xml", true},
		{"text/vnd.anything+xml; charset=utf-8", true},

		{"application/xml+anything", false},
		{"application/xml.anything", false},
		{"application/xml-anything", false},
		{"application/anything.xml", false},
		{"application/anything-xml", false},

		{"text/xml+anything", false},
		{"text/xml.anything", false},
		{"text/xml-anything", false},
		{"text/anything.xml", false},
		{"text/anything-xml", false},
	} {
		t.Equal(tc.exp, xml.Is(tc.ct),
			fmt.Sprintf("tested content type (%s) did not yield expected check state: %t", tc.ct, !tc.exp))
	}
}

func TestXML(t *testing.T) {
	prettytest.RunWithFormatter(
		t,
		new(prettytest.TDDFormatter),
		new(xmlSuite),
	)
}
