package wiwo_test

import (
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/slax0rr/wiwo"
)

type querySuite struct {
	suite.Suite
}

func (t *querySuite) TestGet() {
	r, err := http.NewRequest("GET", "http://localhost/?foo=bar&foo=baz", nil)
	if err != nil {
		t.T().Fatal(err)
	}

	q := wiwo.NewQuery(r)

	for _, tc := range []struct {
		key   string
		value string
	}{
		{
			key:   "foo",
			value: "bar",
		},
		{
			key: "bar",
		},
	} {
		assert.Equal(t.T(), tc.value, q.Get(tc.key))
	}

	assert.Equal(t.T(), []string{"bar", "baz"}, q.GetAll("foo"))
}

func (t *querySuite) TestGetInt() {
	r, err := http.NewRequest("GET", "http://localhost/?foo=1&bar=-1&baz=foo&qux=2147483648", nil)
	if err != nil {
		t.T().Fatal(err)
	}

	q := wiwo.NewQuery(r)

	for _, tc := range []struct {
		key   string
		value int
	}{
		{
			key:   "foo",
			value: 1,
		},
		{
			key:   "bar",
			value: -1,
		},
		{
			key:   "baz",
			value: 0,
		},
		{
			key:   "qux",
			value: 2147483648,
		},
	} {
		assert.Equal(t.T(), tc.value, q.GetInt(tc.key))
	}
}

func (t *querySuite) TestGetAllInt() {
	r, err := http.NewRequest("GET", "http://localhost/?foo=1&foo=-1&foo=foo&foo=2147483648", nil)
	if err != nil {
		t.T().Fatal(err)
	}

	q := wiwo.NewQuery(r)
	assert.EqualValues(t.T(), []int{1, -1, 0, 2147483648}, q.GetAllInt("foo"))
}

func (t *querySuite) TestGetInt32() {
	r, err := http.NewRequest("GET", "http://localhost/?foo=1&bar=-1&baz=foo&qux=2147483648", nil)
	if err != nil {
		t.T().Fatal(err)
	}

	q := wiwo.NewQuery(r)

	for _, tc := range []struct {
		key   string
		value int32
	}{
		{
			key:   "foo",
			value: 1,
		},
		{
			key:   "bar",
			value: -1,
		},
		{
			key:   "baz",
			value: 0,
		},
		{
			key:   "qux",
			value: -2147483648,
		},
	} {
		assert.Equal(t.T(), tc.value, q.GetInt32(tc.key))
	}
}

func (t *querySuite) TestGetAllInt32() {
	r, err := http.NewRequest("GET", "http://localhost/?foo=1&foo=-1&foo=foo&foo=2147483648", nil)
	if err != nil {
		t.T().Fatal(err)
	}

	q := wiwo.NewQuery(r)
	assert.EqualValues(t.T(), []int32{1, -1, 0, -2147483648}, q.GetAllInt32("foo"))
}

func (t *querySuite) TestGetInt64() {
	r, err := http.NewRequest("GET", "http://localhost/?foo=1&bar=-1&baz=foo&qux=2147483648", nil)
	if err != nil {
		t.T().Fatal(err)
	}

	q := wiwo.NewQuery(r)

	for _, tc := range []struct {
		key   string
		value int64
	}{
		{
			key:   "foo",
			value: 1,
		},
		{
			key:   "bar",
			value: -1,
		},
		{
			key:   "baz",
			value: 0,
		},
		{
			key:   "qux",
			value: 2147483648,
		},
	} {
		assert.Equal(t.T(), tc.value, q.GetInt64(tc.key))
	}
}

func (t *querySuite) TestGetAllInt64() {
	r, err := http.NewRequest("GET", "http://localhost/?foo=1&foo=-1&foo=foo&foo=2147483648", nil)
	if err != nil {
		t.T().Fatal(err)
	}

	q := wiwo.NewQuery(r)
	assert.EqualValues(t.T(), []int64{1, -1, 0, 2147483648}, q.GetAllInt64("foo"))
}

func (t *querySuite) TestGetFloat32() {
	r, err := http.NewRequest("GET", "http://localhost/?foo=1&bar=-1&baz=foo&qux=1.1&point=.1", nil)
	if err != nil {
		t.T().Fatal(err)
	}

	q := wiwo.NewQuery(r)

	for _, tc := range []struct {
		key   string
		value float32
	}{
		{
			key:   "foo",
			value: 1.0,
		},
		{
			key:   "bar",
			value: -1.0,
		},
		{
			key:   "baz",
			value: 0.0,
		},
		{
			key:   "qux",
			value: 1.1,
		},
		{
			key:   "point",
			value: 0.1,
		},
	} {
		assert.Equal(t.T(), tc.value, q.GetFloat32(tc.key))
	}
}

func (t *querySuite) TestGetAllFloat32() {
	r, err := http.NewRequest("GET", "http://localhost/?foo=1&foo=-1&foo=foo&foo=1.1&foo=.1", nil)
	if err != nil {
		t.T().Fatal(err)
	}

	q := wiwo.NewQuery(r)
	assert.EqualValues(t.T(), []float32{1.0, -1.0, 0.0, 1.1, 0.1}, q.GetAllFloat32("foo"))
}

func (t *querySuite) TestGetFloat64() {
	r, err := http.NewRequest("GET", "http://localhost/?foo=1&bar=-1&baz=foo&qux=1.1&point=.1", nil)
	if err != nil {
		t.T().Fatal(err)
	}

	q := wiwo.NewQuery(r)

	for _, tc := range []struct {
		key   string
		value float64
	}{
		{
			key:   "foo",
			value: 1.0,
		},
		{
			key:   "bar",
			value: -1.0,
		},
		{
			key:   "baz",
			value: 0.0,
		},
		{
			key:   "qux",
			value: 1.1,
		},
		{
			key:   "point",
			value: 0.1,
		},
	} {
		assert.Equal(t.T(), tc.value, q.GetFloat64(tc.key))
	}
}

func (t *querySuite) TestGetAllFloat64() {
	r, err := http.NewRequest("GET", "http://localhost/?foo=1&foo=-1&foo=foo&foo=1.1&foo=.1", nil)
	if err != nil {
		t.T().Fatal(err)
	}

	q := wiwo.NewQuery(r)
	assert.EqualValues(t.T(), []float64{1.0, -1.0, 0.0, 1.1, 0.1}, q.GetAllFloat64("foo"))
}

func (t *querySuite) TestGetBool() {
	r, err := http.NewRequest("GET", "http://localhost/?foo=1&bar=true&baz=foo&qux=0", nil)
	if err != nil {
		t.T().Fatal(err)
	}

	q := wiwo.NewQuery(r)

	for _, tc := range []struct {
		key   string
		value bool
	}{
		{
			key:   "foo",
			value: true,
		},
		{
			key:   "bar",
			value: true,
		},
		{
			key:   "baz",
			value: false,
		},
		{
			key:   "qux",
			value: false,
		},
	} {
		assert.Equal(t.T(), tc.value, q.GetBool(tc.key))
	}
}

func (t *querySuite) TestGetAllBool() {
	r, err := http.NewRequest("GET", "http://localhost/?foo=1&foo=true&foo=foo&foo=0", nil)
	if err != nil {
		t.T().Fatal(err)
	}

	q := wiwo.NewQuery(r)
	assert.EqualValues(t.T(), []bool{true, true, false, false}, q.GetAllBool("foo"))
}

func (t *querySuite) TestGetTime() {
	testCases := []struct {
		stringVal string
		value     time.Time
	}{
		{
			stringVal: "2020-07-17T16:30:25+00:00",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
		{
			stringVal: "2020-07-17T16:30:25",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
		{
			stringVal: "Fri, 17 Jul 2020 16:30:25 +0000",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
		{
			stringVal: "Fri, 17 Jul 2020 16:30:25 UTC",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
		{
			stringVal: "17 Jul 20 16:30 +0000",
			value:     time.Date(2020, time.July, 17, 16, 30, 0, 0, time.UTC),
		},
		{
			stringVal: "17 Jul 20 16:30 UTC",
			value:     time.Date(2020, time.July, 17, 16, 30, 0, 0, time.UTC),
		},
		{
			stringVal: "Friday, 17-Jul-20 16:30:25 UTC",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
		{
			stringVal: "Fri Jul 17 16:30:25 2020",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
		{
			stringVal: "Fri Jul 17 16:30:25 UTC 2020",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
		{
			stringVal: "Fri Jul 17 16:30:25 +0000 2020",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
		{
			stringVal: "2020-07-17 16:30:25.000001234 +0000 UTC",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 1234, time.UTC),
		},
		{
			stringVal: "2020-07-17",
			value:     time.Date(2020, time.July, 17, 0, 0, 0, 0, time.UTC),
		},
		{
			stringVal: "17 Jul 2020",
			value:     time.Date(2020, time.July, 17, 0, 0, 0, 0, time.UTC),
		},
		{
			stringVal: "2020-07-17T16:30:25+0000",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
		{
			stringVal: "2020-07-17 16:30:25 +00:00",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
		{
			stringVal: "2020-07-17 16:30:25 +0000",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
		{
			stringVal: "2020-07-17 16:30:25+00:00",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
		{
			stringVal: "2020-07-17 16:30:25+0000",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
		{
			stringVal: "2020-07-17 16:30:25",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
		{
			stringVal: "4:30PM",
			value:     time.Date(0, time.January, 1, 16, 30, 0, 0, time.UTC),
		},
		{
			stringVal: "Jul 17 16:30:25",
			value:     time.Date(0, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
		{
			stringVal: "Jul 17 16:30:25.123",
			value:     time.Date(0, time.July, 17, 16, 30, 25, 123000000, time.UTC),
		},
		{
			stringVal: "Jul 17 16:30:25.123456",
			value:     time.Date(0, time.July, 17, 16, 30, 25, 123456000, time.UTC),
		},
		{
			stringVal: "Jul 17 16:30:25.123456789",
			value:     time.Date(0, time.July, 17, 16, 30, 25, 123456789, time.UTC),
		},
		{
			stringVal: "1595003425",
			value:     time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		},
	}

	for _, tc := range testCases {
		p := url.Values{"time": []string{tc.stringVal}}
		r, err := http.NewRequest("GET", "http://localhost/?"+p.Encode(), nil)
		if err != nil {
			t.T().Fatal(err)
		}

		q := wiwo.NewQuery(r)
		tim := q.GetTime("time")
		assert.True(
			t.T(),
			tc.value.Equal(tim),
			"Time (%s) parsed from (%s) does not equal the expected time (%s)",
			tim,
			tc.stringVal,
			tc.value,
		)
	}
}

func (t *querySuite) TestGetAlltime() {
	params := url.Values{
		"foo": []string{
			"2020-07-17T16:30:25+00:00",
			"2020-07-17T16:30:25",
			"Fri, 17 Jul 2020 16:30:25 +0000",
			"Fri, 17 Jul 2020 16:30:25 UTC",
			"17 Jul 20 16:30 +0000",
			"17 Jul 20 16:30 UTC",
			"Friday, 17-Jul-20 16:30:25 UTC",
			"Fri Jul 17 16:30:25 2020",
			"Fri Jul 17 16:30:25 UTC 2020",
			"Fri Jul 17 16:30:25 +0000 2020",
			"2020-07-17 16:30:25.000001234 +0000 UTC",
			"2020-07-17",
			"17 Jul 2020",
			"2020-07-17T16:30:25+0000",
			"2020-07-17 16:30:25 +00:00",
			"2020-07-17 16:30:25 +0000",
			"2020-07-17 16:30:25+00:00",
			"2020-07-17 16:30:25+0000",
			"2020-07-17 16:30:25",
			"4:30PM",
			"Jul 17 16:30:25",
			"Jul 17 16:30:25.123",
			"Jul 17 16:30:25.123456",
			"Jul 17 16:30:25.123456789",
			"1595003425",
		},
	}

	p := params.Encode()
	r, err := http.NewRequest("GET", "http://localhost/?"+p, nil)
	if err != nil {
		t.T().Fatal(err)
	}
	q := wiwo.NewQuery(r)
	parsed := q.GetAllTime("foo")
	exp := []time.Time{
		time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 0, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 0, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 25, 1234, time.UTC),
		time.Date(2020, time.July, 17, 0, 0, 0, 0, time.UTC),
		time.Date(2020, time.July, 17, 0, 0, 0, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
		time.Date(0, time.January, 1, 16, 30, 0, 0, time.UTC),
		time.Date(0, time.July, 17, 16, 30, 25, 0, time.UTC),
		time.Date(0, time.July, 17, 16, 30, 25, 123000000, time.UTC),
		time.Date(0, time.July, 17, 16, 30, 25, 123456000, time.UTC),
		time.Date(0, time.July, 17, 16, 30, 25, 123456789, time.UTC),
		time.Date(2020, time.July, 17, 16, 30, 25, 0, time.UTC),
	}

	for i := range parsed {
		assert.True(
			t.T(),
			exp[i].Equal(parsed[i]),
			"Time (%s) parsed from (%s) does not equal the expected time (%s)",
			parsed[i],
			params["foo"][i],
			exp[i],
		)
	}
}

func (t *querySuite) TestGetDuration() {
	testCases := []struct {
		stringVal string
		value     time.Duration
	}{
		{
			stringVal: "1000",
			value:     time.Duration(1000),
		},
		{
			stringVal: "1000ns",
			value:     time.Duration(1000),
		},
		{
			stringVal: "1000µs",
			value:     time.Microsecond * 1000,
		},
		{
			stringVal: "1000ms",
			value:     time.Millisecond * 1000,
		},
		{
			stringVal: "1000s",
			value:     time.Second * 1000,
		},
		{
			stringVal: "1000m",
			value:     time.Minute * 1000,
		},
		{
			stringVal: "1000h",
			value:     time.Hour * 1000,
		},
	}

	for _, tc := range testCases {
		r, err := http.NewRequest("GET", "http://localhost/?duration="+tc.stringVal, nil)
		if err != nil {
			t.T().Fatal(err)
		}

		q := wiwo.NewQuery(r)
		dur := q.GetDuration("duration")
		assert.Equal(t.T(), tc.value, dur)
	}
}

func (t *querySuite) TestGetAllDuration() {
	params := url.Values{
		"foo": []string{
			"1000",
			"1000ns",
			"1000µs",
			"1000ms",
			"1000s",
			"1000m",
			"1000h",
		},
	}

	r, err := http.NewRequest("GET", "http://localhost/?"+params.Encode(), nil)
	if err != nil {
		t.T().Fatal(err)
	}

	q := wiwo.NewQuery(r)
	assert.EqualValues(t.T(), []time.Duration{
		time.Duration(1000),
		time.Duration(1000),
		time.Microsecond * 1000,
		time.Millisecond * 1000,
		time.Second * 1000,
		time.Minute * 1000,
		time.Hour * 1000,
	}, q.GetAllDuration("foo"))
}

func TestQuery(t *testing.T) {
	suite.Run(t, new(querySuite))
}
